﻿using ServerConnector.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

namespace ServerConnector.Client
{
    public class TCPClientSocket
    {
        TcpClient client = new TcpClient();

        NetworkStream stream;

        public void Connect(string address, int port)
        {
            client.Connect(address, port);
            stream = client.GetStream();
        }
        
        public void CloseConnection()
        {
            stream.Close();
            client.Close();
        }
        
        public void SendPacket(Packet packet)
        {
            MemoryStream s = new MemoryStream();
            s.WriteInt(packet.frame.number);
            packet.Write(s);
            byte[] bytes = s.ToArray();
            //Debug.Log(packet.frame.number + " " + bytes.Length);
            stream.WriteInt(bytes.Length);
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
           // Debug.Log("Sended!");
        }
        
        public Packet TryRead()
        {
            if (stream != null && stream.DataAvailable)
            {
                Debug.Log("READED");
                int frame = stream.ReadInt();
                byte[] bytes = new byte[frame];
                stream.Read(bytes, 0, bytes.Length);
                MemoryStream mem = new MemoryStream(bytes);
                int nr = mem.ReadInt();
                Packet p = PacketManager.GetClientBoundPacket(new PacketFrame(nr));
                p.Read(mem);
                return p;
            }
            else
            {
                return null;
            }
            
        }
    }
}
