﻿using ServerConnector.Client.ClientBound;
using ServerConnector.Client.ServerBound;
using ServerConnector.Packets;
using ServerConnector.Packets.ClientBound;
using ServerConnector.Packets.LoginBound;
using ServerConnector.Packets.ServerBound;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

namespace ServerConnector.Client
{
    /// <summary>
    /// Base class to initialize connection from, handle connections and get game data from
    /// </summary>
    public class ClientConnector
    {

        private TCPClientSocket authServer, client;

        private ServerBoundPacketProcessor ServerBoundProcessor = new ServerBoundPacketProcessor();
        // private LoginPacketProcessor LoginPacketProcessor = new LoginPacketProcessor();

        private int Token = 0;
        private String email;
        /// <summary>
        /// UUID of the player, it's unique for every player registered in system
        /// </summary>
        public int Uuid { get; private set; }
        
        private readonly string AuthServerAddress = "192.168.2.106";
        private readonly int AuthServerPort = 5555;


        private int Port;
        private string Address;
        /// <summary>
        /// Display Name of current user
        /// </summary>
        public string Username { get; private set; }
        /// <summary>
        /// Determines if player is logged in
        /// </summary>
        public bool LoggedIn { get; internal set; }

        /// <summary>
        /// Determines if player authorized himself
        /// </summary>
        public bool Authorized { get; internal set; }
        /// <summary>
        /// If current Client is closed and shouldn't be used
        /// </summary>
        public bool Closed { get; private set; }
        /// <summary>
        /// Version of client
        /// </summary>
        public readonly int Version;

        /// <summary>
        /// Contains all definitions of players
        /// </summary>
        public SerializedPlayerData[] Players { get; internal set; }

        public OwnData OwnData { get; internal set; }

        public int PlayerWithExecution { get; internal set; }
        public int PlotCardVersion { get; internal set; }
        public int CharactersVersion { get; internal set; }

        private byte[] lenght = new byte[4];

        private bool Queued = false;


        /// <summary>
        /// Contains all in-system events
        /// </summary>
        public readonly ActionManager ActionsManager = new ActionManager();

        public void Login(string email, string password)
        {
            //TODO: Remove
            //Players = new SerializedPlayerData[2];

            //TODO: Login
            this.email = email;
            authServer = new TCPClientSocket();
            authServer.Connect(AuthServerAddress, AuthServerPort);
            P01LoginPacket l = new P01LoginPacket()
            {
                email = email,
                password = password
            };
            authServer.SendPacket(l);
            Debug.Log("SENDED");
        }

        public bool Accepted { get; internal set; }

        internal void ConnectToServer(string address, int port)
        {
            client = new TCPClientSocket();
            client.Connect(address, port);
            Closed = false;
            SendLoginData();
        }

        private void ProcessAuthServer(Packet packet)
        {
            if (packet is P22LoginStatus)
            {
                P22LoginStatus p = (P22LoginStatus)packet;
                if (p.success)
                {
                    Token = p.token;
                    Uuid = p.uuid;
                    Username = p.username;
                    Debug.Log(Token + " " + Uuid + " " + Username);
                    LoggedIn = true;
                    ActionsManager.LoginSuccessful.Invoke();
                }
                else
                {
                    ActionsManager.LoginFailed.Invoke();
                }
            }
            else if (packet is P23QueueJoinAccept)
            {
                Accepted = true;
                Queued = true;
                ActionsManager.QueueJoinEvent.Invoke();
            }
            else if (packet is P24QueueLeftAccept)
            {
                Queued = false;
                ActionsManager.QueueLeftEvent.Invoke();
            }
            else if (packet is P25GameFound)
            {
                LeftQueue();
                P25GameFound p = (P25GameFound)packet;
                ActionsManager.GameFoundEvent.Invoke();
                ConnectToServer(p.address, p.port);
            }
            else if (packet is P27TokenError)
            {
                Token = 0;
                ActionsManager.TokenError.Invoke();
            }else if(packet is P30Confirm)
            {
                Accepted = true;
            }
        }

        public void JoinQueue()
        {
            P02QueueJoin queue = new P02QueueJoin()
            {
                token = Token,
                uuid = Uuid
            };
            Debug.Log(queue.token + " " + queue.uuid + " QUEUE");
            SendAuthPacket(queue);
        }

        public void LeftQueue()
        {
            P03QueueLeft p = new P03QueueLeft()
            {
                token = Token,
                uuid = Uuid
            };
            SendAuthPacket(p);
            Queued = false;
        }

        /// <summary>
        /// Closes connection
        /// </summary>
        public void CloseConnection()
        {
            if (Queued)
            {
                LeftQueue();
            }
            if (!Closed)
            {
                Closed = true;
                client.CloseConnection();
            }
            if (authServer != null)
            {
                authServer.CloseConnection();
            }

        }
        /// <summary>
        /// Refreshes last packet indicator
        /// </summary>
        internal void ServerKeepAlive()
        {
            lastPacket = CurrentTimeMilis;
        }

        private void RefreshToken()
        {
            SendAuthPacket(new P05RefreshToken()
            {
                token = Token,
                uuid = Uuid
            });
        }

        long lastPacket = CurrentTimeMilis;

        public ClientConnector(int version)
        {
            Version = version;
            Closed = true;
        }

        internal void SendAuthPacket(Packet p)
        {
            authServer.SendPacket(p);
        }

        internal void SendClientPacket(Packet p)
        {
            client.SendPacket(p);
        }

        internal static long CurrentTimeMilis { get { return (long)((DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds); } }

        private long LastTickExecute = CurrentTimeMilis, LastTokenRefresh = CurrentTimeMilis, LastQueueTick = CurrentTimeMilis;

        /// <summary>
        /// Tick of the server, must be executed at least ones per 300ms (~3 ticks/second)
        /// </summary>
        /// <param name="debug">Nothing, set as null if you need something here</param>
        public void Tick(object debug = null)
        {
            if (authServer != null)
            {
                Packet p = authServer.TryRead();
                if (p != null)
                {
                    ProcessAuthServer(p);
                }
            }
            if (client != null)
            {
                Packet cp = client.TryRead();
                if (cp != null)
                {
                    ServerBoundProcessor.ProcessPacket(cp, this);
                }
            }


            if (CurrentTimeMilis - LastTickExecute <= 300)
            {
                return;
            }
            LastTickExecute = CurrentTimeMilis;
            if (Token != 0 && CurrentTimeMilis - LastTokenRefresh >= 420000)
            {
                RefreshToken();
            }
            if (Queued && Accepted && CurrentTimeMilis - LastQueueTick >= 900)
            {
                Accepted = false;
                LastQueueTick = CurrentTimeMilis;
                SendAuthPacket(new P04QueueCheck()
                {
                    token = Token,
                    uuid = Uuid
                });
            }
            if (!Closed && Token != 0 && LoggedIn && CurrentTimeMilis - lastPacket >= 300)
            {
                SendClientPacket(new P01ClientKeepAlivePacket());
            }
            //Session keep-alive
        }
        /// <summary>
        /// Refreshes internal gamedata  (cards number, game finish)
        /// </summary>
        public void RefreshGameData()
        {
            SendClientPacket(new P02GameDataRequest());
        }

        /// <summary>
        /// Refreshes internal own player data
        /// </summary>
        public void RefreshOwnData()
        {
            Debug.Log("RefOD");
            SendClientPacket(new P04OwnDataRequest());
        }
        /// <summary>
        /// Refreshes internal players data
        /// </summary>
        public void RefreshPlayersData()
        {
            Debug.Log("RefPD");
            SendClientPacket(new P03PlayersDataRequest());
        }

        /// <summary>
        /// Uses plot card
        /// </summary>
        /// <param name="card">Card to use</param>
        /// <param name="target">Target of card, can be null</param>
        public void UsePlotCard(int card, SerializedPlayerData target = null)
        {
            P06PlotCardUse plotcard = new P06PlotCardUse()
            {
                target = (target != null ? target.uuid : 0),
                card = card
            };
            SendClientPacket(plotcard);
        }

        /// <summary>
        /// Uses passive
        /// </summary>
        /// <param name="target">Target of passive, can be null</param>
        public void UsePassive(SerializedPlayerData target = null)
        {
            P05PassiveUse passive = new P05PassiveUse()
            {
                target = (target != null ? target.uuid : 0)
            };
            SendClientPacket(passive);
        }

        /// <summary>
        /// When player have Execution Card, he can claim death
        /// </summary>
        public void ClaimDeath()
        {
            SendClientPacket(new P07ClaimDeath());
        }

        /// <summary>
        /// Indicates if game started
        /// </summary>
        public bool GameStarted { get; private set; }

        internal void ServerAction(Actions action)
        {
            if (action == Actions.PRE_GAME_START)
            {
                RefreshGameData();
                ActionsManager.PreGameStartEvent.Invoke();
            }
            else if (action == Actions.GAME_STARTED)
            {
                GameStarted = true;
                RefreshPlayersData();
                RefreshOwnData();
                ActionsManager.GameStartEvent.Invoke();
            }
            else if (action == Actions.ROUND_END)
            {
                ActionsManager.RoundEndEvent.Invoke();
            }
            else if (action == Actions.EXECUTION_END)
            {
                ActionsManager.ExecutionEndEvent.Invoke();
            }
        }

        internal SerializedPlayerData GetPlayerByUUID(int player)
        {
            foreach (SerializedPlayerData pl in Players)
            {
                if (pl.uuid == player)
                {
                    return pl;
                }
            }
            return null;

        }

        internal void SendLoginData()
        {
            client.SendPacket(new P18LoginData()
            {
                data = new LoginData(email, Token, Username, Uuid)
            });
        }
    }

    public class OwnData : SerializedPlayerData
    {
        public readonly List<int> plotcards;

        public OwnData(int currentCharacter, int coins, int[] plotcards, bool died, int uuid, string username)
        {
            this.characterCard = currentCharacter;
            this.coins = coins;
            this.plotcards = new List<int>(plotcards);
            this.died = died;
            this.numberOfPlotCards = plotcards.Length;
            this.uuid = uuid;
            this.username = username;
        }
    }

    /// <summary>
    /// Manages all internal actions
    /// Register own delegates for game
    /// </summary>
    public class ActionManager
    {

        private void NullExecutor() { }
        private void NullExecutor(int o) { }
        private void NullExecutor(SerializedPlayerData pl, int a) { }
        private void NullExecutor(SerializedPlayerData pl, int a, int b) { }
        private void NullExecutor(SerializedPlayerData pl) { }
        private void NullExecutor(SerializedPlayerData pl, SerializedPlayerData a) { }
        private void NullExecutor(SerializedPlayerData pl, SerializedPlayerData a, int b) { }
        private void NullExecutor(bool b) { }

        /// <summary>
        /// This construction is only to prevent NullPointerException
        /// </summary>
        public ActionManager()
        {
            ConnectionSuccessfull = NullExecutor;
            ConnectionFailed = NullExecutor;
            LoginSuccessful = NullExecutor;
            LoginFailed = NullExecutor;
            VersionNotMatch = NullExecutor;
            PreGameStartEvent = NullExecutor;
            GameStartEvent = NullExecutor;
            CashModifyEvent = NullExecutor;
            CharacterReshuffleEvent = NullExecutor;
            DeathEvent = NullExecutor;
            ExecutionMoveEvent = NullExecutor;
            PassiveUseEvent = NullExecutor;
            PlotCardAddEvent = NullExecutor;
            OwnPlotCardAddEvent = NullExecutor;
            PlotCardUseEvent = NullExecutor;
            WinEvent = NullExecutor;
            CharacterChangeEvent = NullExecutor;
            PlayersDataUpdate = NullExecutor;
            OwnDataUpdate = NullExecutor;
            QueueJoinEvent = NullExecutor;
            GameFoundEvent = NullExecutor;
            QueueLeftEvent = NullExecutor;
            RoundEndEvent = NullExecutor;
            ExecutionEndEvent = NullExecutor;
        }

        /// <summary>
        /// Called when connection was successfull
        /// </summary>
        public Action ConnectionSuccessfull { internal get; set; }
        /// <summary>
        /// Called when connection to server failed
        /// </summary>
        public Action ConnectionFailed { internal get; set; }
        /// <summary>
        /// Called when Login was successfull
        /// </summary>
        public Action LoginSuccessful { internal get; set; }
        /// <summary>
        /// Called when Login failed
        /// </summary>
        public Action LoginFailed { internal get; set; }
        /// <summary>
        /// Called when versions don't match
        /// Argument is version of server
        /// </summary>
        public Action<int> VersionNotMatch { internal get; set; }
        /// <summary>
        /// Called when server starts initializing game
        /// </summary>
        public Action PreGameStartEvent { internal get; set; }

        /// <summary>
        /// Called when all people logged it. It's time to start!
        /// </summary>
        public Action GameStartEvent { internal get; set; }

        /// <summary>
        /// Called when player changes money
        /// Arg1: Player in which money changed
        /// Arg2: Final result of cash modification
        /// </summary>
        public Action<SerializedPlayerData, int> CashModifyEvent { internal get; set; }

        /// <summary>
        /// Called when characters are reshuffled (redraw)
        /// Arg1: True if it's random, false if it's normal
        /// </summary>
        public Action<bool> CharacterReshuffleEvent { internal get; set; }

        /// <summary>
        /// Called when someone died
        /// Arg1: Killer
        /// Arg2: Killed
        /// </summary>
        public Action<SerializedPlayerData, SerializedPlayerData> DeathEvent { internal get; set; }

        /// <summary>
        /// Called when someone moves execution
        /// Arg1: Player who had execution
        /// Arg2: Player who has execution now
        /// </summary>
        public Action<SerializedPlayerData, SerializedPlayerData> ExecutionMoveEvent { internal get; set; }

        /// <summary>
        /// Called when player uses passive
        /// Arg1: Executor
        /// Arg2: Receiver, can be null
        /// Arg3: Card used
        /// </summary>
        public Action<SerializedPlayerData, SerializedPlayerData, int> PassiveUseEvent { internal get; set; }

        /// <summary>
        /// When player receives new card
        /// Arg1: Player who gets one more card
        /// </summary>
        public Action<SerializedPlayerData> PlotCardAddEvent { internal get; set; }

        /// <summary>
        /// When connected player gets card
        /// Arg1: Card received
        /// </summary>
        public Action<int> OwnPlotCardAddEvent { internal get; set; }

        /// <summary>
        /// Called when user uses card
        /// Arg1: Executor of card
        /// Arg2: Receiver of card, can be null
        /// Arg3: Card used
        /// </summary>
        public Action<SerializedPlayerData, SerializedPlayerData, int> PlotCardUseEvent { internal get; set; }

        /// <summary>
        /// Called when player wins game
        /// Arg1: Winner
        /// </summary>
        public Action<SerializedPlayerData> WinEvent { internal get; set; }

        /// <summary>
        /// Called when player exchange character
        /// Arg1: Player, who has his card changed
        /// Arg2: Previous card, can be 0
        /// Arg3: Current card
        /// </summary>
        public Action<SerializedPlayerData, int, int> CharacterChangeEvent { internal get; set; }

        /// <summary>
        /// Called when players data is updated
        /// </summary>
        public Action PlayersDataUpdate { internal get; set; }

        /// <summary>
        /// Called when own data is updated
        /// </summary>
        public Action OwnDataUpdate { internal get; set; }

        /// <summary>
        /// Called when starting searching game
        /// </summary>
        public Action QueueJoinEvent { internal get; set; }

        /// <summary>
        /// Called when found game
        /// </summary>
        public Action GameFoundEvent { internal get; set; }

        /// <summary>
        /// Called whem player left for searching game
        /// </summary>
        public Action QueueLeftEvent { internal get; set; }

        /// <summary>
        /// When Token Error occurred and have to relog again
        /// </summary>
        public Action TokenError { internal get; set; }

        /// <summary>
        /// Called when player card is deleted
        /// </summary>
        public Action<SerializedPlayerData, int> PlotCardDeleteEvent { internal get; set; }

        public Action RoundEndEvent { internal get; set; }

        public Action ExecutionEndEvent { internal get; set; }

    }

    /// <summary>
    /// Manages packets
    /// </summary>
    internal class PacketManager
    {
        public readonly static Dictionary<int, Type> ClientBound = new Dictionary<int, Type>();

        static PacketManager()
        {
            //LoginBound.Add(1, typeof(P01Handshake));
            //LoginBound.Add(2, typeof(P02LoginStart));
            //LoginBound.Add(3, typeof(P03LoginData));
            //LoginBound.Add(4, typeof(P04LoginStatus));
            //LoginBound.Add(5, typeof(P05ModeChange));

            ClientBound.Add(1, typeof(P01ClientKeepAlivePacket));
            ClientBound.Add(2, typeof(P02PlayersData));
            ClientBound.Add(3, typeof(P03GameData));
            ClientBound.Add(4, typeof(P04OwnData));
            ClientBound.Add(5, typeof(P05ActionEvent));
            ClientBound.Add(6, typeof(P06CashModifyEvent));
            ClientBound.Add(7, typeof(P07CharacterReshuffleEvent));
            ClientBound.Add(8, typeof(P08DeathEvent));
            ClientBound.Add(9, typeof(P09ExecutionGiveEvent));
            ClientBound.Add(10, typeof(P10PassiveUseEvent));
            ClientBound.Add(11, typeof(P11PlotCardAddEvent));
            ClientBound.Add(12, typeof(P12OwnPlotCardAddEvent));
            ClientBound.Add(13, typeof(P13PlotCardUseEvent));
            ClientBound.Add(14, typeof(P14WinEvent));
            ClientBound.Add(15, typeof(P15CharacterChangeEvent));
            ClientBound.Add(16, typeof(P16PlotCardDeleteEvent));
            ClientBound.Add(17, typeof(P17LoginStart));
            ClientBound.Add(18, typeof(P18LoginData));
            ClientBound.Add(19, typeof(P19LoginStatus));
            ClientBound.Add(20, typeof(P20ModeChange));
            ClientBound.Add(21, typeof(P21Handshake));
            ClientBound.Add(22, typeof(P22LoginStatus));
            ClientBound.Add(23, typeof(P23QueueJoinAccept));
            ClientBound.Add(24, typeof(P24QueueLeftAccept));
            ClientBound.Add(25, typeof(P25GameFound));
            ClientBound.Add(26, typeof(P26TokenRefresh));
            ClientBound.Add(27, typeof(P27TokenError));
            ClientBound.Add(30, typeof(P30Confirm));

        }

        internal static Packet GetClientBoundPacket(PacketFrame frame)
        {
            Type t = ClientBound[frame.number];
            if (t != null)
            {
                Packet p = (Packet)Activator.CreateInstance(t);
                p.frame = frame;
                return p;
            }
            return null;
        }
    }

    public class PacketFrame
    {

        public int number;
        public int length;
        public PacketFrame(int number, int length = 0)
        {
            this.number = number;
            this.length = length;
        }

        public PacketFrame()
        {
        }

    }
    /// <summary>
    /// PACKET MUST HAVE EMPTY CONSTRUCTOR
    /// </summary>
    public abstract class Packet
    {

        public PacketFrame frame;

        public Packet(PacketFrame frame = null)
        {
            this.frame = frame;
        }

        /// <summary>
        /// Problem: Numbers are big endian. You must convert them
        /// </summary>
        /// <param name="buffer"></param>
        public abstract void Read(Stream buffer);

        /// <summary>
        /// Problem: Numbers are big endian. You must convert them
        /// </summary>
        public abstract void Write(Stream buffer);

        public abstract int GetLength();
    }

    public abstract class ServerBoundPacket : Packet
    {
        public ServerBoundPacket(PacketFrame frame) : base(frame) { }
        public ServerBoundPacket(int i) : base(new PacketFrame(i)) { }
        public override void Read(Stream buffer) { }
        public override int GetLength() { return 0; }
    }

    public abstract class ClientBoundPacket : Packet
    {
        public ClientBoundPacket(PacketFrame frame) : base(frame) { }
        public ClientBoundPacket(int i) : base(new PacketFrame(i)) { }
        public override void Write(Stream buffer) { }
        public override int GetLength() { return 0; }
    }
    internal static class PacketMutator
    {
        internal static byte[] GetPacketAsBytes(this Packet packet)
        {
            MemoryStream stream = new MemoryStream();

            byte[] b = packet.frame.number.ConvertedArrayBytes();
            stream.Write(b, 0, b.Length);

            b = packet.GetLength().ConvertedArrayBytes();
            stream.Write(b, 0, b.Length);

            packet.Write(stream);
            return stream.ToArray();
        }

        internal static Packet ReadPacket(byte[] header, Stream stream, bool auth)
        {
            byte[] m = new byte[4];

            Array.Copy(header, 0, m, 0, 4);
            int number = m.Int32ConvertedFromBytes();

            Array.Copy(header, 4, m, 0, 4);
            int length = m.Int32ConvertedFromBytes();
            Packet p = PacketManager.GetClientBoundPacket(new PacketFrame(number, length));
            p.Read(stream);
            return p;
        }
    }

    internal class ServerBoundPacketProcessor
    {
        //TODO: Modification 
        internal void ProcessPacket(Packet p, ClientConnector conn)
        {
            conn.ServerKeepAlive();
            Debug.Log("Processing " + p.GetType().Name);
            if (p is P01ServerKeepAlivePacket)
            {
                Debug.Log("Keeped alive!");
            }
            else if (p is P02PlayersData)
            {
                conn.Players = ((P02PlayersData)p).Players;
                conn.PlayerWithExecution = ((P02PlayersData)p).Execution;
            }
            else if (p is P03GameData)
            {
                var packet = (P03GameData)p;
                conn.CharactersVersion = packet.characterCardsVersion;
                conn.PlotCardVersion = packet.plotCardsVersion;
            }
            else if (p is P04OwnData)
            {
                var packet = ((P04OwnData)p);
                conn.OwnData = packet.ToOwnData();
                for (int i = 0; i < conn.Players.Length; i++)
                {
                    if (conn.Players[i].uuid == conn.OwnData.uuid)
                    {
                        conn.Players[i] = conn.OwnData;
                    }
                }
            }
            else if (p is P05ActionEvent)
            {
                var packet = ((P05ActionEvent)p);
                conn.ServerAction(packet.action);
            }
            else if (p is P06CashModifyEvent)
            {
                var packet = ((P06CashModifyEvent)p);
                SerializedPlayerData player = conn.GetPlayerByUUID(packet.player);
                player.coins = packet.finalCoins;
                conn.ActionsManager.CashModifyEvent(player, packet.finalCoins);
            }
            else if (p is P07CharacterReshuffleEvent)
            {
                var packet = ((P07CharacterReshuffleEvent)p);
                conn.ActionsManager.CharacterReshuffleEvent.Invoke(packet.isRandom);
            }
            else if (p is P08DeathEvent)
            {
                var packet = ((P08DeathEvent)p);
                SerializedPlayerData killed = conn.GetPlayerByUUID(packet.killed);
                killed.died = true;
                conn.ActionsManager.DeathEvent.Invoke(conn.GetPlayerByUUID(packet.killer), killed);
            }
            else if (p is P09ExecutionGiveEvent)
            {
                var packet = ((P09ExecutionGiveEvent)p);
                conn.PlayerWithExecution = packet.to;
                conn.ActionsManager.ExecutionMoveEvent.Invoke(conn.GetPlayerByUUID(packet.from), conn.GetPlayerByUUID(packet.to));
            }
            else if (p is P10PassiveUseEvent)
            {
                var packet = ((P10PassiveUseEvent)p);
                conn.ActionsManager.PassiveUseEvent.Invoke(conn.GetPlayerByUUID(packet.executor), conn.GetPlayerByUUID(packet.receiver), packet.card);
            }
            else if (p is P11PlotCardAddEvent)
            {
                var packet = ((P11PlotCardAddEvent)p);
                SerializedPlayerData player = conn.GetPlayerByUUID(packet.player);
                player.numberOfPlotCards++;
                conn.ActionsManager.PlotCardAddEvent(player);
            }
            else if (p is P12OwnPlotCardAddEvent)
            {
                var packet = ((P12OwnPlotCardAddEvent)p);
                conn.OwnData.plotcards.Add(packet.card);
                conn.ActionsManager.OwnPlotCardAddEvent(packet.card);
            }
            else if (p is P13PlotCardUseEvent)
            {
                var packet = ((P13PlotCardUseEvent)p);
                conn.ActionsManager.PlotCardUseEvent.Invoke(conn.GetPlayerByUUID(packet.executor), conn.GetPlayerByUUID(packet.receiver), packet.card);
            }
            else if (p is P14WinEvent)
            {
                var packet = (P14WinEvent)p;
                conn.ActionsManager.WinEvent.Invoke(conn.GetPlayerByUUID(packet.player));
                conn.CloseConnection();
            }
            else if (p is P15CharacterChangeEvent)
            {
                var packet = (P15CharacterChangeEvent)p;
                conn.GetPlayerByUUID(packet.player).characterCard = packet.to;
                conn.ActionsManager.CharacterChangeEvent.Invoke(conn.GetPlayerByUUID(packet.player), packet.from, packet.to);
            }
            else if (p is P16PlotCardDeleteEvent)
            {
                var packet = (P16PlotCardDeleteEvent)p;
                SerializedPlayerData data = conn.GetPlayerByUUID(packet.player);
                data.numberOfPlotCards--;
                if (data is OwnData)
                {
                    ((OwnData)data).plotcards.Remove(packet.card);
                }
                conn.ActionsManager.PlotCardDeleteEvent.Invoke(conn.GetPlayerByUUID(packet.player), packet.card);
            }
            else if (p is P19LoginStatus)
            {
                var login = p as P19LoginStatus;
                if (login.loginSuccessfull)
                {
                    Debug.Log("Logged IN!");
                    conn.LoggedIn = true;
                    conn.ActionsManager.ConnectionSuccessfull.Invoke();
                }
                else
                {
                    Debug.Log("Sorry, connection unsuccessful!");
                    conn.CloseConnection();
                }
            }
        }
    }

    public class SerializedPlayerData
    {

        public string username;
        public int uuid;
        public bool died;
        public int characterCard;
        public int coins, numberOfPlotCards;

        public SerializedPlayerData()
        {
        }

        public SerializedPlayerData(string username, int uuid, bool died, int characterCard, int coins, int numberOfPlotCards)
        {
            this.username = username;
            this.uuid = uuid;
            this.died = died;
            this.characterCard = characterCard;
            this.coins = coins;
            this.numberOfPlotCards = numberOfPlotCards;
        }

    }

    public class Auth
    {

        public bool successfull;

        public int uuid;
        public int token;
        public String username;

        public Auth()
        {
            this.successfull = false;
        }

        public Auth(int uuid, int token, String username)
        {
            this.successfull = true;
            this.uuid = uuid;
            this.token = token;
            this.username = username;
        }

    }


    public class QueueStatus
    {

        public enum Mode
        {

            QUEUED = 0, WAITING_FOR_LOGIN = 1, SEARCHING = 2, FOUND = 3, FAILED = 4

        }

        public QueueStatus(Mode mode)
        {
            this.mode = mode;
        }

        public QueueStatus(int token, int uuid, Mode mode)
        {
            this.token = token;
            this.uuid = uuid;
            this.mode = mode;
        }

        public int token, uuid;

        public Mode mode;

        public String address;
        public int port;

        public QueueStatus()
        {
        }

    }
}
