﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ServerConnector
{
    public static class Extensions
    {
        public static string[] ToStringArray(this byte[] array)
        {
            string[] s = new string[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                s[i] = array[i].ToString();
            }
            return s;
        }

        public static byte[] ConvertedArrayBytes(this long number)
        {
            byte[] bytes = BitConverter.GetBytes(number);
            Array.Reverse(bytes);
            return bytes;
        }

        public static byte[] ConvertedArrayBytes(this int number)
        {
            byte[] bytes = BitConverter.GetBytes(number);
            Array.Reverse(bytes);
            return bytes;
        }

        public static int Int32ConvertedFromBytes(this byte[] array, int start = 0)
        {
            Array.Reverse(array);
            return BitConverter.ToInt32(array, start);
        }

        public static Int64 Int64ConvertedFromBytes(this byte[] array, int start = 0)
        {
            Array.Reverse(array);
            return BitConverter.ToInt64(array, start);
        }
        public static int ReadInt(this Stream stream)
        {
            byte[] bytes = new byte[4];
            stream.Read(bytes, 0, 4);
            return bytes.Int32ConvertedFromBytes();
        }
        public static long ReadLong(this Stream strea)
        {
            byte[] bytes = new byte[8];
            strea.Read(bytes, 0, 8);
            return bytes.Int64ConvertedFromBytes();
        }
        public static string ReadString(this Stream stream)
        {
            int length = stream.ReadInt();
            byte[] bytes = new byte[length];
            stream.Read(bytes, 0, length);
            return Encoding.UTF8.GetString(bytes);
        }
        public static bool ReadBool(this Stream stream)
        {
            int i = stream.ReadByte();
            return (i == 1);
        }
        public static void WriteInt(this Stream stream, int i)
        {
            byte[] bytes = i.ConvertedArrayBytes();
            stream.Write(bytes, 0, 4);
        }
        public static void WriteString(this Stream stream, string i)
        {
            byte[] st = Encoding.UTF8.GetBytes(i);
            stream.WriteInt(st.Length);
            stream.Write(st, 0, st.Length);
        }
        
    }

    
}
