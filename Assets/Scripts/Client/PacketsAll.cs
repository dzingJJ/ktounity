﻿using ServerConnector.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ServerConnector.Packets
{
    namespace ServerBound
    {
        public class P01ClientKeepAlivePacket : ServerBoundPacket
        {
            public P01ClientKeepAlivePacket() : base(new PacketFrame(1)) { }

            public override int GetLength()
            {
                return 0;
            }

            public override void Write(Stream buffer)
            { }
        }

        public class P02GameDataRequest : ServerBoundPacket
        {
            public P02GameDataRequest() : base(new PacketFrame(2)) { }

            public override int GetLength()
            {
                return 0;
            }

            public override void Write(Stream buffer)
            {
            }
        }

        public class P03PlayersDataRequest : ServerBoundPacket
        {
            public P03PlayersDataRequest() : base(new PacketFrame(3))
            {
            }

            public override int GetLength()
            {
                return 0;
            }

            public override void Write(Stream buffer)
            { }
        }

        public class P04OwnDataRequest : ServerBoundPacket
        {
            public P04OwnDataRequest() : base(new PacketFrame(4))
            {
            }

            public override int GetLength()
            {
                return 0;
            }

            public override void Write(Stream buffer)
            { }
        }

        public class P05PassiveUse : ServerBoundPacket
        {
            public int target;

            public P05PassiveUse() : base(new PacketFrame(5))
            {
            }

            public override int GetLength()
            {
                return 4;
            }

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(target);
            }
        }

        public class P06PlotCardUse : ServerBoundPacket
        {
            public int target, card;

            public P06PlotCardUse() : base(new PacketFrame(6))
            {
            }

            public override int GetLength()
            {
                return 8;
            }

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(target);
                buffer.WriteInt(card);
            }
        }

        public class P07ClaimDeath : ServerBoundPacket
        {
            public P07ClaimDeath() : base(new PacketFrame(7))
            {
            }

            public override int GetLength()
            {
                return 0;
            }

            public override void Write(Stream buffer)
            { }
        }
    }

    namespace ClientBound
    {
        public class P01ServerKeepAlivePacket : ClientBoundPacket
        {
            public P01ServerKeepAlivePacket() : base(new PacketFrame(1)) { }

            public override void Read(Stream buffer)
            { }

            public override int GetLength()
            {
                return 0;
            }
        }




        public class P02PlayersData : ClientBoundPacket
        {
            public SerializedPlayerData[] Players;
            public int Execution;

            public P02PlayersData() : base(new PacketFrame(2)) { }

            public override void Read(Stream buffer)
            {
                Execution = buffer.ReadInt();
                Players = new SerializedPlayerData[buffer.ReadInt()];
                for (int i = 0; i < Players.Length; i++)
                {
                    string username = buffer.ReadString();
                    int uuid = buffer.ReadInt(), card = buffer.ReadInt(), coins = buffer.ReadInt(), numberOfPlotCards = buffer.ReadInt();
                    bool died = buffer.ReadBool();
                    Players[i] = new SerializedPlayerData(username, uuid, died, card, coins, numberOfPlotCards);
                }
            }
        }

        public class P03GameData : ClientBoundPacket
        {
            public int characterCardsVersion, plotCardsVersion;
            public bool hasStarted;

            public P03GameData() : base(new PacketFrame(3))
            {
            }

            public override void Read(Stream buffer)
            {
                characterCardsVersion = buffer.ReadInt();
                plotCardsVersion = buffer.ReadInt();
                hasStarted = buffer.ReadBool();
            }
        }

        public class P04OwnData : ClientBoundPacket
        {
            public int currentCharacter, coins;
            public int[] plotcards;
            public bool died;
            public int uuid;
            public string username;

            public P04OwnData() : base(new PacketFrame(4))
            {
            }

            public override void Read(Stream buffer)
            {
                currentCharacter = buffer.ReadInt();
                coins = buffer.ReadInt();
                died = buffer.ReadBool();
                int len = buffer.ReadInt();
                plotcards = new int[len];
                for (int i = 0; i < len; i++)
                {
                    plotcards[i] = buffer.ReadInt();
                }
                uuid = buffer.ReadInt();
                username = buffer.ReadString();
            }

            public OwnData ToOwnData()
            {
                return new OwnData(currentCharacter, coins, plotcards, died, uuid, username);
            }
        }

        public class P05ActionEvent : ClientBoundPacket
        {
            public P05ActionEvent() : base(new PacketFrame(5)) { }
            public override void Read(Stream buffer)
            {
                action = (Actions)buffer.ReadInt();
            }

            public Actions action;


        }
        public enum Actions : int
        {
            /// <summary>
            /// When error occured, should never occure
            /// </summary>
            UNKNOWN = 0,
            /// <summary>
            /// Game should start downloading data
            /// </summary>
            GAME_STARTED = 1,
            /// <summary>
            /// Game should start loading on this event
            /// </summary>
            PRE_GAME_START = 2,

            ROUND_END = 3,

            EXECUTION_END = 4
        }

        public class P06CashModifyEvent : ClientBoundPacket
        {
            public P06CashModifyEvent() : base(new PacketFrame(6))
            {
            }

            public int finalCoins;
            public int player;

            public override void Read(Stream buffer)
            {
                player = buffer.ReadInt();
                finalCoins = buffer.ReadInt();
            }
        }

        public class P07CharacterReshuffleEvent : ClientBoundPacket
        {
            public P07CharacterReshuffleEvent() : base(new PacketFrame(7))
            {
            }
            public bool isRandom;

            public override void Read(Stream buffer)
            {
                isRandom = buffer.ReadBool();
            }
        }

        public class P08DeathEvent : ClientBoundPacket
        {
            public int killer, killed;

            public P08DeathEvent() : base(new PacketFrame(8))
            {
            }

            public override void Read(Stream buffer)
            {
                killer = buffer.ReadInt();
                killed = buffer.ReadInt();
            }
        }

        public class P09ExecutionGiveEvent : ClientBoundPacket
        {

            public int from, to;

            public P09ExecutionGiveEvent() : base(new PacketFrame(9))
            {
            }

            public override void Read(Stream buffer)
            {
                from = buffer.ReadInt();
                to = buffer.ReadInt();
            }
        }

        public class P10PassiveUseEvent : ClientBoundPacket
        {
            public int executor, receiver, card;

            public P10PassiveUseEvent() : base(new PacketFrame(10))
            {
            }

            public override void Read(Stream buffer)
            {
                executor = buffer.ReadInt();
                receiver = buffer.ReadInt();
                card = buffer.ReadInt();
            }
        }

        public class P11PlotCardAddEvent : ClientBoundPacket
        {
            public P11PlotCardAddEvent() : base(new PacketFrame(11)) { }
            public int player;
            public override void Read(Stream buffer)
            {
                player = buffer.ReadInt();
            }
        }

        public class P12OwnPlotCardAddEvent : ClientBoundPacket
        {
            public P12OwnPlotCardAddEvent() : base(new PacketFrame(12)) { }

            public int card;

            public override void Read(Stream buffer)
            {
                card = buffer.ReadInt();
            }
        }

        public class P13PlotCardUseEvent : ClientBoundPacket
        {
            public int executor, receiver, card;

            public P13PlotCardUseEvent() : base(new PacketFrame(13))
            {
            }

            public override void Read(Stream buffer)
            {
                executor = buffer.ReadInt();
                receiver = buffer.ReadInt();
                card = buffer.ReadInt();
            }
        }

        public class P14WinEvent : ClientBoundPacket
        {
            public int player;

            public P14WinEvent() : base(new PacketFrame(14))
            {
            }

            public override void Read(Stream buffer)
            {
                player = buffer.ReadInt();
            }
        }

        public class P15CharacterChangeEvent : ClientBoundPacket
        {
            public int player, from, to;

            public P15CharacterChangeEvent() : base(new PacketFrame(15))
            {
            }

            public override void Read(Stream buffer)
            {
                player = buffer.ReadInt();
                from = buffer.ReadInt();
                to = buffer.ReadInt();
            }
        }

        public class P16PlotCardDeleteEvent : ClientBoundPacket
        {
            public int player;
            public int card;

            public P16PlotCardDeleteEvent() : base(new PacketFrame(16))
            {
            }

            public override void Read(Stream buffer)
            {
                player = buffer.ReadInt();
                card = buffer.ReadInt();
            }
        }
    }

    namespace LoginBound
    {

        public class P21Handshake : Packet
        {
            public int version;

            public P21Handshake(int version) : base(new PacketFrame(21)) { this.version = version; }
            public override int GetLength()
            {
                return 4;
            }

            public override void Read(Stream buffer)
            {
            }

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(version);
            }
        }

        public class P17LoginStart : Packet
        {
            public int serverVersion;
            public P17LoginStart() : base(new PacketFrame(17)) { }
            public override int GetLength()
            {
                return 0;
            }

            public override void Read(Stream buffer)
            {
                serverVersion = buffer.ReadInt();
            }

            public override void Write(Stream buffer)
            {
            }
        }
        public class LoginData
        {

            public string email, username;
            public int uuid, token;

            public LoginData(string email, int token, string username, int uuid)
            {
                this.email = email;
                this.token = token;
                this.username = username;
                this.uuid = uuid;
            }

            public LoginData()
            {
            }
        }
        public class P18LoginData : Packet
        {
            public P18LoginData() : base(new PacketFrame(18)) { }
            public override int GetLength()
            {
                return 4 + data.email.Length + 1 + 4 + 1 + data.username.Length;
            }

            public LoginData data;

            public P18LoginData(LoginData data) : base(new PacketFrame(3))
            {
                this.data = data;
            }

            public override void Read(Stream buffer)
            {
                int uuid = buffer.ReadInt();
                int token = buffer.ReadInt();
                string username = buffer.ReadString();
                string email = buffer.ReadString();
                data = new LoginData(email, token, username, uuid);
            }

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(data.uuid);
                buffer.WriteInt(data.token);
                buffer.WriteString(data.username);
                buffer.WriteString(data.email);
            }

        }

        public class P19LoginStatus : Packet
        {

            public P19LoginStatus() : base(new PacketFrame(19)) { }
            /// <summary>
            /// 1 if successfull
            /// 2 if failed
            /// </summary>
            public bool loginSuccessfull;

            public override int GetLength()
            {
                return 1;
            }

            public override void Read(Stream buffer)
            {
                int b = buffer.ReadByte();
                if (b == 1)
                {
                    loginSuccessfull = true;
                }
            }

            public override void Write(Stream buffer)
            {
                if (loginSuccessfull)
                    buffer.WriteByte(1);
                else
                    buffer.WriteByte(2);
            }
        }

        public class P20ModeChange : Packet
        {
            public P20ModeChange() : base(new PacketFrame(20)) { }
            public override int GetLength()
            {
                return 0;
            }

            public override void Read(Stream buffer)
            {
            }

            public override void Write(Stream buffer)
            {
            }
        }
    }

}
namespace ServerConnector.Client
{
    namespace ClientBound
    {

        public class P22LoginStatus : ClientBoundPacket
        {

            public bool success;
            public int token, uuid;
            public String username;


            public P22LoginStatus() : base(new PacketFrame(22))
            {
            }

            public override void Read(Stream buffer)
            {
                success = buffer.ReadBool();
                token = buffer.ReadInt();
                uuid = buffer.ReadInt();
                username = buffer.ReadString();
            }
        }

        public class P23QueueJoinAccept : ClientBoundPacket
        {
            public P23QueueJoinAccept() : base(23) { }

            public override void Read(Stream buffer)
            { }
        }

        public class P24QueueLeftAccept : ClientBoundPacket
        {
            public P24QueueLeftAccept() : base(24) { }

            public override void Read(Stream buffer)
            { }
        }

        public class P25GameFound : ClientBoundPacket //TODO
        {
            public P25GameFound() : base(25) { }

            public String address;
            public int port;

            public override void Read(Stream buffer)
            {
                address = buffer.ReadString();
                port = buffer.ReadInt();
            }
        }

        public class P26TokenRefresh : ClientBoundPacket
        {
            public P26TokenRefresh() : base(26) { }

            public bool success;

            public override void Read(Stream buffer)
            {
                success = buffer.ReadBool();
            }
        }

        public class P27TokenError : ClientBoundPacket
        {
            public P27TokenError() : base(27) { }

            public override void Read(Stream buffer)
            { }
        }
        public class P30Confirm : ClientBoundPacket
        {
            public P30Confirm() : base(30) { }
            public override void Read(Stream buffer)
            { }
        }
    }
    namespace ServerBound
    {
        public class P01LoginPacket : ServerBoundPacket
        {
            public P01LoginPacket() : base(1) { }

            public String email, password;

            public override void Write(Stream buffer)
            {
                buffer.WriteString(email);
                buffer.WriteString(password);
            }
        }

        public class P02QueueJoin : ServerBoundPacket
        {
            public P02QueueJoin() : base(2) { }

            public int token, uuid;

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(token);
                buffer.WriteInt(uuid);
            }
        }

        public class P03QueueLeft : ServerBoundPacket
        {
            public P03QueueLeft() : base(3) { }

            public int token, uuid;

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(token);
                buffer.WriteInt(uuid);
            }
        }

        public class P04QueueCheck : ServerBoundPacket
        {
            public P04QueueCheck() : base(4) { }

            public int token, uuid;

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(token);
                buffer.WriteInt(uuid);
            }
        }

        public class P05RefreshToken : ServerBoundPacket
        {
            public P05RefreshToken() : base(5) { }

            public int token, uuid;

            public override void Write(Stream buffer)
            {
                buffer.WriteInt(token);
                buffer.WriteInt(uuid);
            }
        }
    }
}
