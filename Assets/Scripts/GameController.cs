﻿using ServerConnector.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable()]
public class CharacterData
{
    public int uuid;
    public byte[] character;
    public int characterX;
    public int characterY;
}

public class GameController : MonoBehaviour
{
    public Sprite background;

    public Sprite[] Overlord = new Sprite[3];
    public Sprite[] General = new Sprite[3];
    public Sprite[] Peasant = new Sprite[3];

    public Sprite[] cursor = new Sprite[2];

    public float fontSize = 0.2f;
    public GameObject preset;
    
    string email = "JJ";
    string password = "pass";

    GameObject[] img;
    GameObject[] hand = new GameObject[4];
    GameObject[] player;
    GameObject[] playerData;
    GameObject[] died;
    int[] id;
    int[] death;
    int deathnum = 0;
    Vector3[] spritePos;
    GameObject currentGameObject;
    int num = 0;
    Vector3 offset;
    Vector3 origin = new Vector3(-7, 2.5f, 10);
    Vector3 yourPos = new Vector3(0, -2.5f, 10);
    float addPosition;

    GameObject description;
    GameObject descbackground;

    ClientConnector client = new ClientConnector(1);

    bool isGameStarted = false;
    int pNumber;

    private void l(object o)
    {
        Debug.Log(o);
    }

    void ConnectionSuccessful()
    {
        Debug.Log("Connection successful");
    }

    void ConnectionFailed()
    {
        Debug.Log("Connection failed");
    }

    void LoginSuccessful()
    {
        Debug.Log("Login successful");
        client.JoinQueue();
    }

    void LoginFailed()
    {
        Debug.Log("Login failed");
    }

    void VersionNotMatch(int serverVersion)
    {
        Debug.Log("Version doesn't match");
    }

    void PreGameStartEvent()
    {
        
    }

    void GameStartEvent()
    {
        l(client.Players);

        id = SetUUID(client.Players.Length);

        pNumber = client.Players.Length;

        GenerateGameBoard(pNumber);
    }

    void CashModifyEvent(SerializedPlayerData pData, int finalCash)
    {
        playerData[UUIDToNumber(pData.uuid, id, pNumber)].GetComponent<TextMesh>().text = "Cards: " + pData.numberOfPlotCards + "\nCards: " + pData.numberOfPlotCards;
    }

    void CharacterReshuffleEvent(bool ifRandom)
    {

    }

    void DeathEvent(SerializedPlayerData killer, SerializedPlayerData victim)
    {
        death[UUIDToNumber(victim.uuid, id, pNumber)] = ++deathnum;
        died[UUIDToNumber(victim.uuid, id, pNumber)] = Instantiate(preset);
        died[UUIDToNumber(victim.uuid, id, pNumber)].AddComponent<TextMesh>();
        died[UUIDToNumber(victim.uuid, id, pNumber)].GetComponent<TextMesh>().text = "DIED [" + deathnum + "]";
        died[UUIDToNumber(victim.uuid, id, pNumber)].GetComponent<TextMesh>().color = new Color32(200, 30, 30, 0);
        died[UUIDToNumber(victim.uuid, id, pNumber)].transform.SetParent(player[UUIDToNumber(victim.uuid, id, pNumber)].transform);
        died[UUIDToNumber(victim.uuid, id, pNumber)].transform.localPosition = new Vector3(0, 2);
    }

    void ExecutionMoveEvent(SerializedPlayerData giver, SerializedPlayerData receiver)
    {

    }

    void PassiveUseEvent(SerializedPlayerData executor, SerializedPlayerData receiver, int cardID)
    {

    }

    void PlotCardAddEvent(SerializedPlayerData receiver)
    {

    }

    void OwnPlotCardAddEvent(int cardID)
    {

    }

    void PlotCardUseEvent(SerializedPlayerData executor, SerializedPlayerData receiver, int cardID)
    {

    }

    void WinEvent(SerializedPlayerData winner)
    {
        Environment.Exit(0);
    }

    void CharacterChangeEvent(SerializedPlayerData player, int previousCharID, int newCharID)
    {

    }

    void PlayersDataUpdate()
    {

    }

    void OwnDataUpdate()
    {

    }

    void QueueErrorEvent()
    {

    }

    void QueueJoinEvent()
    {

    }

    void GameFoundEvent()
    {

    }

    void QueueLeftEvent()
    {

    }

    private void RoundEndEvent()
    {
        
    }

    private void ExecutionEndEvent()
    {
        
    }

    private void PlotCardDeleteEvent(SerializedPlayerData arg1, int arg2)
    {
        
    }

    private void TokenError()
    {
        
    }

    void RegisterClientEvents()
    {
        client.ActionsManager.ConnectionSuccessfull = ConnectionSuccessful;
        client.ActionsManager.ConnectionFailed = ConnectionFailed;
        client.ActionsManager.LoginSuccessful = LoginSuccessful;
        client.ActionsManager.LoginFailed = LoginFailed;
        client.ActionsManager.VersionNotMatch = VersionNotMatch;
        client.ActionsManager.PreGameStartEvent = PreGameStartEvent;
        //client.ActionsManager.GameStartEvent = GameStartEvent;
        client.ActionsManager.CashModifyEvent = CashModifyEvent;
        client.ActionsManager.CharacterReshuffleEvent = CharacterReshuffleEvent;
        client.ActionsManager.DeathEvent = DeathEvent;
        client.ActionsManager.ExecutionMoveEvent = ExecutionMoveEvent;
        client.ActionsManager.PassiveUseEvent = PassiveUseEvent;
        client.ActionsManager.PlotCardAddEvent = PlotCardAddEvent;
        client.ActionsManager.OwnPlotCardAddEvent = OwnPlotCardAddEvent;
        client.ActionsManager.PlotCardUseEvent = PlotCardUseEvent;
        client.ActionsManager.WinEvent = WinEvent;
        client.ActionsManager.CharacterChangeEvent = CharacterChangeEvent;
        client.ActionsManager.PlayersDataUpdate = PlayersDataUpdate;
        client.ActionsManager.OwnDataUpdate = OwnDataUpdate;
        client.ActionsManager.ExecutionEndEvent = ExecutionEndEvent;
        client.ActionsManager.RoundEndEvent = RoundEndEvent;
        client.ActionsManager.QueueJoinEvent = QueueJoinEvent;
        client.ActionsManager.GameFoundEvent = GameFoundEvent;
        client.ActionsManager.QueueLeftEvent = QueueLeftEvent;
        client.ActionsManager.TokenError = TokenError;
        client.ActionsManager.PlotCardDeleteEvent = PlotCardDeleteEvent;
    }

    void SetCursor(Sprite sprite)
    {
        Cursor.SetCursor(sprite.texture, new Vector2(sprite.texture.width / 2, sprite.texture.height / 2), CursorMode.Auto);
    }

    GameObject GetYourHand(int cN) // TODO | Optimization
    {
        GameObject card = Instantiate(preset);
        card.tag = "Card";
        BoxCollider2D collider = card.AddComponent<BoxCollider2D>();
        TextMesh text = card.AddComponent<TextMesh>();
        text.text = "Card no. " + cN;
        text.characterSize = 0.2f;
        collider.size = new Vector2(1.2f, 0.2f);
        collider.offset = new Vector2(0.6f, -0.15f);
        return card;
    }

    void SetOwnData()
    {
        for (int i = 0; i < 4; i++)
        {
            hand[i] = GetYourHand(i);
            hand[i].transform.SetParent(player[0].transform);
            hand[i].transform.localPosition = new Vector3(-0.5f, -0.5f - (0.25f * i), 10);
        }
    }

    void RefreshPlayerData(GameObject playerData, GameObject player, GameObject[] img, int playerPosition, Vector3[] spritePos, SerializedPlayerData playerClient, bool isColliderInstantiated = true)
    {
        TextMesh text = playerData.GetComponent<TextMesh>();
        text.text = "Cards: " + 4 + "\nCash: " + 5;
        text.characterSize = fontSize;
        playerData.transform.position = Vector3.zero;
        playerData.transform.SetParent(player.transform);
        playerData.transform.localPosition = new Vector3(-0.35f, 0.1f, 10);
        if (!isColliderInstantiated)
        {
            playerData.AddComponent<BoxCollider2D>();
            isColliderInstantiated = true;
        }
        BoxCollider2D collider = playerData.GetComponent<BoxCollider2D>();
        collider.size = playerData.GetComponent<Renderer>().bounds.size;
        playerData.tag = "Card";

        for (int j = 0; j < 3; j++)
        {
            if (playerClient.characterCard == 8)
            {
                img[num].GetComponent<SpriteRenderer>().sprite = Overlord[j];
            }
            if (playerClient.characterCard == 5)
            {
                img[num].GetComponent<SpriteRenderer>().sprite = General[j];
            }
            if (playerClient.characterCard == 9)
            {
                img[num].GetComponent<SpriteRenderer>().sprite = Peasant[j];
            }
            img[num].transform.SetParent(player.transform);
            img[num].transform.localPosition = spritePos[j];
            if (img[num].transform.localScale.x < 2)
                img[num].transform.localScale *= 2;
            if (j == 0)
            {
                img[num].tag = "Player";
            }
            num++;
        }
    }

    void SetPlayerData(int pN/*, SerializedPlayerData[] players*/)
    {
        img = new GameObject[pN * 3];
        for (int j = 0; j < pN * 3; j++)
        {
            img[num] = Instantiate(preset);
            img[num].AddComponent<SpriteRenderer>();
            num++;
        }

        num = 0;
        for (int i = 0; i < pN; i++)
        {
            player[i] = Instantiate(preset);
            if (i == 0)
            {
                player[i].transform.position = yourPos;
            }
            else { 
                player[i].transform.position = origin + new Vector3(addPosition * (i - 1), 0, 0);
            }

            playerData[i] = Instantiate(preset);
            playerData[i].AddComponent<TextMesh>();

            spritePos = new Vector3[3];
            spritePos[0] = new Vector3(0, 1, 10);
            spritePos[1] = new Vector3(0.6f, 1.5f, 10);
            spritePos[2] = new Vector3(-0.6f, 1.5f, 10);

            l(id[i]);
            RefreshPlayerData(playerData[i], player[i], img, i, spritePos, client.Players[UUIDToNumber(id[i], id, pN)], false);

        }
        num = 0;
    }

    void GenerateGameBoard(int playerNumber)
    {
        if (playerNumber > 2)
            addPosition = 14 / (playerNumber - 2);
        else
            addPosition = 8;
        id = new int[playerNumber];
        death = new int[playerNumber];
        died = new GameObject[pNumber];
        player = new GameObject[playerNumber];
        playerData = new GameObject[playerNumber];
        img = new GameObject[playerNumber * 3];

        // TODO | Queue
        GameObject half = Instantiate(preset);
        half.AddComponent<SpriteRenderer>();
        half.GetComponent<SpriteRenderer>().sprite = background;
        half.transform.position = new Vector3(0, 0, 10);
        half.transform.localScale = new Vector2(1800, 10); //(new Vector2(2f * Camera.main.orthographicSize * Camera.main.aspect, 0.35f));

        SetPlayerData(playerNumber/*, client.Players*/);

        SetOwnData();
        num = 0;
    }

    void Description(RaycastHit2D hit)
    {
        if (hit.collider && description == null && !Input.GetMouseButton(0))
        {
            description = Instantiate(preset);
            TextMesh text = description.AddComponent<TextMesh>();
            text.text = hit.collider.gameObject.GetComponent<TextMesh>().text;
            text.characterSize = 0.3f;
            descbackground = Instantiate(preset);
            SpriteRenderer renderer = descbackground.AddComponent<SpriteRenderer>();
            renderer.sprite = background;
            renderer.drawMode = SpriteDrawMode.Tiled;
            descbackground.transform.position = new Vector3(description.GetComponent<Renderer>().bounds.size.x / 2, -description.GetComponent<Renderer>().bounds.size.y / 2, 5);
            descbackground.transform.SetParent(description.transform);
            description.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 10);
            renderer.size = description.GetComponent<Renderer>().bounds.size * 1.2f;
        }
        else if (hit.collider && description != null && !Input.GetMouseButton(0))
        {
            description.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 10);
        }
        else
        {
            Destroy(descbackground);
            Destroy(description);
            description = null;
        }
    }

    void MouseCheck(RaycastHit2D hit)
    {
        if (Input.GetMouseButtonUp(0))
        {
            SetCursor(cursor[0]);

            RefreshPlayerData(playerData[0], player[0], img, 0, spritePos, client.Players[id[0]]);

            num = 0;
        }
        if (Input.GetMouseButtonDown(0))
        {
            SetCursor(cursor[1]);
            if (hit.collider && currentGameObject == null)
            {
                if (hit.collider.gameObject.CompareTag("Card"))
                {
                    currentGameObject = hit.collider.gameObject;
                }
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    int nr = 0;
                    for(int i = 0; i < pNumber; i++)
                    {
                        if (player[i].Equals(hit.collider.gameObject))
                        {
                            nr = i;
                            break;
                        }
                    }
                    RefreshPlayerData(playerData[nr], hit.collider.gameObject, img, nr, spritePos, client.Players[id[nr]]);
                }
            }
        }
    }

    int UUIDToNumber(int uuid, int[] ids, int playersNumber = 3)
    {
        int i = 0;
        while (ids[i] != uuid)
        {
            i++;
        }
        return i;
    }

    int[] SetUUID(int max = 3)
    {
        int[] idList = new int[max];
        for (int i = 0; i < max; i++)
        {
            idList[i] = client.Players[i].uuid;
        }
        return idList;
    }

    // Use this for initialization
    void Start()
    {
        Camera.main.backgroundColor = new Color32(85, 40, 40, 0);

        SetCursor(cursor[0]);
    }
    
    private void OnGUI()
    {
        if (!isGameStarted)
        {
            email = GUI.TextField(new Rect(Screen.width / 2, Screen.height / 2, 50, 20), email);
            password = GUI.TextField(new Rect(Screen.width / 2, Screen.height / 2 + 30, 50, 20), password);
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 + 60, 50, 20), "Login"))
            {
                Login();
            }
        }
    }

    void Login()
    {
        RegisterClientEvents();
        client.Login(email, password);
    }

    // Update is called once per frame
    void Update()
    {
        client.Tick();

        if (!isGameStarted && client.Players != null && client.OwnData != null)
        {
            GameStartEvent();
            isGameStarted = true;
        }

        RaycastHit2D h = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        Description(h);

        MouseCheck(h);
    }
}
